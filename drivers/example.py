# Example printer class

# This is an example of the printer class that the queue progam expects to
# import for simulation/testing purposes. Please note that this class may
# include some extra variables or functions exclusivly for the purpose of
# simulation.

# The most basic version of the class is:

# class Printer:
# 	def print(self, file)
# 	def isidle(self)
#	def check_valid_file(self, file)

import os

class Printer:
	def __init__(self, conn):
		self.conn = conn
		self.idle_count = 0
		self.idle_max = 5

	def print(self, file):
		print("Simulating print job for " + file)
		print("Resetting idle counter")
		self.idle_count = 0
		return True;

	def isIdle(self):
		if self.idle_count >= self.idle_max:
			print("Idle check success")
			return True
		else:
			print("Idle has not reached count yet, %d of %d" % \
				(self.idle_count, self.idle_max))
			self.idle_count = self.idle_count + 1
			return False;

	def isValidFile(self, file):
		# This is a test, so no actual checks are needed.
		return file != ''


# Printer class for the Da Vinci Mini

# I don't want to try and re-write the interaction process with that printer,
# so this will be a wrapper around [minimover][1]. The program has a command line
# and GUI version, so putting the command line version in the path should get
# this working.
#
# [1]: https://github.com/reality-boy/miniMover

import subprocess

class Printer:
	def __init__(self, conn):
		self.conn = conn

	def print(self, file):
		subprocess.run(["minimover", "-d", self.conn, "-p", file], shell=True)

	def isIdle(self):
		proc = subprocess.run(["minimover", "-d", self.conn, "-r"], capture_output=True, text=True, shell=True)
		return "j:9511" in proc.stdout

	def isValidFile(self, file):
		return (file.endswith(".3w") or file.endswith(".gcode"))


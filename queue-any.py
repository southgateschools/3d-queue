#!/usr/bin/python3

import os, sys, time
import argparse


# Define some vars
waittime = 5


# Function to return the first empty dir in a list
def getemptydir(dirs):
	for d in dirs:
		if len(os.listdir(d)) == 0:
			return d
	return False


# Function to get the next file in the queue
def getqueuedfile(dir):
	files = os.listdir(dir)
	if len(files) > 0:
		return files[0]
	else:
		return False


def main():
	# Get the command line args
	parser = argparse.ArgumentParser()
	parser.add_argument("-d", metavar="DIR", required=True, help="Queue directory to get files from")
	parser.add_argument("dirs", nargs="+", help="Queues to put files into")
	args = parser.parse_args()

	# Infinite Loop
	while True:

		# Get a file from the queue
		fn = getqueuedfile(args.d)

		# Check if any of the destination queues are empty
		emptyd = getemptydir(args.dirs)

		# If so…
		if fn and emptyd:

			# Put the file in there
			os.rename(args.d + '/' + fn, emptyd + '/' + fn)

		else:

			# Otherwise, wait and loop again
			time.sleep(waittime)


if __name__ == "__main__":
	main()



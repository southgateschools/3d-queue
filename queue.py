#!/usr/bin/python3

import os, sys, time
import importlib
import argparse


# Define some vars
waittime = 5


# Function to return a valid file from a directory queue.
def get_file(dir, check):
	#Get all files from the queue, and filter by acceptable types
	files = [dir + '/' + f for f in os.listdir(dir) if check(dir + '/' + f)]
	if len(files) > 0:
		return files[0]
	else:
		return ''


# Function to import the printer class from a given driver file.
def load_printer(driver, conn):
	mod = importlib.import_module("drivers." + driver)
	cls = getattr(mod, "Printer")
	return cls(conn)


def main():
	# Get the command line arguments
	parser = argparse.ArgumentParser()
	parser.add_argument("-d", metavar="DIR", required=True, help="Queue directory to watch")
	parser.add_argument("-p", metavar="DRIVER", required=True, help="Printer driver to use")
	parser.add_argument("-c", metavar="CONNECTION", required=True, help="Printer connection info")
	args = parser.parse_args()

	# Load the printer configuration
	printer = load_printer(
		driver = args.p,
		conn = args.c )

	# Infinite Loop
	while True:

		# Get a file from the queue
		file = get_file(args.d, printer.isValidFile)

		# If we got a file, and the printer is ready…
		if (file != '' and printer.isIdle()):

			# Send the file to the printer
			printer.print(file)

			# Wait for the printer to finish…
			while not printer.isIdle():
				time.sleep(waittime)

			# Remove the print job from the list
			os.remove(file)

		# Wait a bit and go again.
		time.sleep(waittime)


if __name__ == "__main__":
	main()



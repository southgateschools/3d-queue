# MiniMaker 3D Printer Queue

This project is designed to set up a system to automate printing across
multiple [Da Vinci Mini 3D Printers][1]. Theorectically, it could actually be
generalized into a queueing system for nearly anything, but the focus in this
case is the 3d printers.


## Design

A 3d printer will be given a designated directory to use as a queue. As slice
files ([gcode][2], 3w, etc) are placed in that directory, the software should
grab one of the files and feed it to the 3d printer. Once the job is done and
the printer indicates that it's idle, the software will delete that slice file
and start again with another slice file. One directory and instance of this
sofware will run for each printer, making it able to work with any number of
printers.

In case you have a lot of jobs and it does not matter what printer the job is
sent to, another program will exist to act as a generalized queue for all of
the printers. When a slice file is placed in the directory that this process
watches, it will take the file and put it into the printer queue that has no
files in it. If no queues are empty, it will wait until one of them is. This
prevents job time from being unevenly distributed among printers, as well as
give print jobs for specific printers priority by keeping the queue occupied.

The end result will be a directory structure that looks something like this:

- queue/
	- any/
	- 1/
	- 2/
	- 3/

…assuming that you have 3 printer queues labeled as `1`, `2`, and `3`, with
the generalized queue running as `any`.


## Usage

General usage is documented when running the program with the `--help` flag.
Run one instance of the `queue.py` program for each printer queue. For example:

```
python3 queue.py -d /tmp/queue -p example -c 3
```

…will start a queue that watches the folder `/tmp/queue` and talks to a 3d
printer using the [`example`](drivers/example.py) driver on connection `3`.

It's suggested to write a wrapper script to start up each printer queue and the
'any' queue since it's pretty annoying to manually do it each time.


## Driver Files

The software uses a "driver" file to talk with the different potential types of
3d pritners. The driver file is actually a python program that provides a
`Printer` class that has methods specialized for working with that particular
type of printer. The [`example.py`](drivers/example.py) driver shows the
methods needed for the the `Printer` class to work.

Because you could have multiple of the same kind of printer, a "connection"
string is passed to the class that should hopefully differenciate one from
another. Because of the differnt types of printers this could handle, as well
as working with Windows and Linux, there is no specified standard for what that
connection string should be. If no connection string is needed, one still must
be passed.



[1]: https://web.archive.org/web/20210323011554/https://www.xyzprinting.com/en-US/product/da-vinci-minimaker
[2]: https://en.wikipedia.org/wiki/G-code
[3]: https://web.archive.org/web/20210511182752/http://wiki.xyzprinting.com/xyzmaker/en/export/

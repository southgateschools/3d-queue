#!/usr/bin/python3

import unittest
import os
import queue
import tempfile



def mktfiles(dir):
	if not os.path.isdir(dir):
		os.mkdir(dir)
	for fn in ["good.gcode", \
			"good.3w", \
			"3w.bad", \
			"bad.gcode.bad", \
			"bad", \
			"good.good.3w"]:
		path = dir + '/' + fn
		with open(path, 'w') as fh:
			fh.write(path)


def rmtfiles(dir):
	for fn in os.listdir(dir):
		os.remove(dir + '/' + fn)
	os.rmdir(dir)


class GetFileTest(unittest.TestCase):
	def setUp(self):
		self.tdir = tempfile.mkdtemp()
		mktfiles(self.tdir)

	def tearDown(self):
		rmtfiles(self.tdir)

	def test_get_file(self):
		while True:
			# Get a list of the test files
			files = os.listdir(self.tdir)

			# Exit when there are no more files
			if len(files) == 0:
				break

			# Get a file from the test dir
			result = queue.get_file(self.tdir, \
				lambda x: x.endswith(".3w") or x.endswith(".gcode"))

			# Check if there are any valid files left
			left = [m for m in files if "good" in m]
			if len(left) > 0:
				self.assertIn("good", result, msg=result)
				self.assertIn(self.tdir, result, msg=result)
			else:
				self.assertEqual(result, '', msg=result)

			# Remove one of the files and go again
			os.remove(self.tdir + '/' + files[0])

		# Return an empty string when there are no files in the dir
		result = queue.get_file(self.tdir, lambda x: true)
		self.assertEqual(result, '', msg="Empty dir")



#!/usr/bin/python3

import unittest
import os
import queue
import tempfile

class PrinterLoadTest(unittest.TestCase):
	def test_load_printer(self):
		# Attempt to load the example printer config
		p = queue.load_printer(conf="example", conn="1")
		for m in ["print", "isIdle", "isValidFile"]:
			self.assertTrue(hasattr(p, m), msg=m + " method")

